QUEST
===================================



Edit common/config/main-local.php to edit connection

'db' => [
                    'class' => 'apaoww\oci8\Oci8DbConnection',
                    'dsn' => 'oci8:dbname=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521))(CONNECT_DATA=(SID=xe)));charset=AL32UTF8;', // Oracle
                    'username' => 'databaseusername',
                    'password' => 'databasepassword',
                    'attributes' => [
                        PDO::ATTR_STRINGIFY_FETCHES => true,
                    ]
                ],