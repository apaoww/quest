<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'apaoww\DbManagerOci8\DbManager', // or use 'yii\rbac\DbManager'
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-advanced-app'
                ],
            ],
        ],
    ],
    'aliases' => [
        '@apaoww/DbManagerOci8' => '@vendor/apaoww/yii2-dbmanager-oci8',
        '@apaoww/AdminOci8' => '@vendor/apaoww/yii2-admin-oci8',
    ],
];
