<?php

use yii\db\Schema;
use yii\db\Migration;
require_once("Autoincrement.php");
class m150318_052308_create_cuba_migrate_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%cuba_migrate}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        Autoincrement::up('cuba_migrate', $this->db->driverName);

    }

    public function down()
    {
         Autoincrement::down('cuba_migrate', $this->db->driverName);
        $this->dropTable('{{%cuba_migrate}}');
    }
}
