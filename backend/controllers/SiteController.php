<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $start_num = 100;
        $end_num = 1000;
        $query = (new Query)
            ->from('auth_item')
            ->where(['type' => 1]);
        $connection = \Yii::$app->db;
        $demoPackage = $connection->createCommand('DECLARE
                                                      start_num NUMBER;
                                                      end_num NUMBER;

                                                    BEGIN
                                                      start_num := '.$start_num.';
                                                      end_num := '.$end_num.';
                                                      DEMOPACKAGE.INSERT_NUMBER(start_num, end_num);
                                                    END;')->query();
        $users = $connection->createCommand('SELECT MY_NUM FROM EVEN_TAB')->queryAll();

        return $this->render('index',
            ['auth_item'=>$query->all(),'users'=>$users]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
