<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "EC_MQA_PROPOSAL".
 *
 * @property string $MQA_PROPOSAL_ID
 * @property string $ACAD_ID
 * @property string $PART_B_1
 * @property string $PART_B_3
 * @property string $PART_B_8
 * @property string $PART_B_10
 * @property string $PART_B_11
 * @property string $PART_B_12
 * @property string $PART_B_13
 * @property string $PART_B_14
 * @property string $PART_B_15
 * @property string $PART_B_16
 * @property string $PART_B_17
 * @property string $PART_B_18
 * @property string $PART_B_19
 * @property string $PART_C_AREA1_1_1_1_AIM
 * @property string $PART_C_AREA1_1_1_1_OTHERS
 * @property string $PART_C_AREA1_1_1_2_MISSION
 * @property string $PART_C_AREA1_1_1_2_SHOWHOW
 * @property string $PART_C_AREA1_1_1_3_JUSTFICTION
 * @property string $PART_C_AREA1_1_1_3_MARKETNEED
 * @property string $PART_C_AREA1_1_1_3_RELATE
 * @property string $PART_C_AREA1_1_1_4_INCORPORATE
 * @property string $PART_B_2
 * @property string $PART_B_4
 * @property string $PART_B_5
 * @property string $PART_B_7
 * @property string $PART_B_9
 * @property string $PART_C_AREA1_INDICATOR
 * @property string $PART_C_AREA1_1_1_5_CONSULTED
 * @property string $PART_C_AREA1_1_2_3_ACHIEVEMENT
 * @property string $PART_C_AREA1_1_2_4_LEARNING
 * @property string $PART_C_AREA2_INDICATOR
 * @property string $PART_C_AREA2_2_1_1_PROVISIONS
 * @property string $PART_C_AREA2_2_1_3_AUTONOMY
 * @property string $PART_C_AREA2_2_1_4_POLICIES
 * @property string $PART_C_AREA2_2_1_5_HEP_PLANS
 * @property string $PART_C_AREA2_2_1_5_DEPT_ROLE
 * @property string $PART_C_AREA2_2_2_1_PROCESS
 * @property string $PART_C_AREA2_2_2_2_TEACHGMTHOD
 * @property string $PART_C_AREA2_2_2_4_INQUIRY
 * @property string $PART_C_AREA2_2_2_5_LEARNGMTHOD
 * @property string $PART_C_AREA2_2_2_6_MULTI
 * @property string $PART_C_AREA2_2_2_7_EXTERNAL
 * @property string $PART_C_AREA2_2_2_8_ACTIVITIES
 * @property string $PART_C_AREA2_2_3_1_SUBJECTS
 * @property string $PART_C_AREA2_2_3_3_COURSE
 * @property string $PART_C_AREA2_2_3_4_PLAN
 * @property string $PART_C_AREA2_2_3_5_EVIDENCE
 * @property string $PART_C_AREA2_2_4_1_SAMPLE
 * @property string $PART_C_AREA2_2_4_2_MANNER
 * @property string $PART_C_AREA2_2_4_3_DESGNATION
 * @property string $PART_C_AREA2_2_4_4_REVIEW
 * @property string $PART_C_AREA2_2_4_5_LEARNING
 * @property string $PART_C_AREA2_2_4_6_INITIATIVE
 * @property string $PART_C_AREA2_2_4_7_ENGAGES
 * @property string $PART_C_AREA2_2_5_1_LINKS
 * @property string $PART_C_AREA2_2_5_2_MECHNISM
 * @property string $PART_C_AREA3_3_3_10_COPY
 * @property string $PART_C_AREA3_3_3_11_IMPROVING
 * @property string $PART_C_AREA3_3_3_12_AUTONMIES
 * @property string $PART_C_AREA3_3_3_13_NATURE
 * @property string $PART_C_AREA4_INDICATOR
 * @property string $PART_C_AREA4_4_1_1_RESPONSBLE
 * @property string $PART_C_AREA4_4_1_2_EVIDENCE
 * @property string $PART_C_AREA4_4_1_3_ADMISSION
 * @property string $PART_C_AREA4_4_1_4_CRITERIA
 * @property string $PART_C_AREA4_4_1_5_APPEAL
 * @property string $PART_C_AREA4_4_1_6_CHRCTERSTCS
 * @property string $PART_C_AREA4_4_1_7_FORECAST
 * @property string $PART_C_AREA4_4_1_8_SELECTION
 * @property string $PART_C_AREA4_4_1_9_INTERVIEW
 * @property string $PART_C_AREA4_4_1_10_SPECIAL
 * @property string $PART_C_AREA4_4_1_11_MONITOR
 * @property string $PART_C_AREA4_4_1_12_ENGAGE
 * @property string $PART_C_AREA4_4_1_13_RELTIONSHP
 * @property string $PART_C_AREA4_4_2_1_POLICIES
 * @property string $PART_C_AREA4_4_2_2_DVLOPMENT
 * @property string $PART_C_AREA4_4_3_1_POLICY
 * @property string $PART_C_AREA4_4_3_2_ACCEPTED
 * @property string $PART_C_AREA4_4_3_3_FACILITIES
 * @property string $PART_C_AREA4_4_4_1_SUPPORT
 * @property string $PART_C_AREA4_4_4_2_ACCESSBLITY
 * @property string $PART_C_AREA4_4_4_3_COMPLAIN
 * @property string $PART_C_AREA4_4_4_4_ADEQUACY
 * @property string $PART_C_AREA4_4_4_5_ROLES
 * @property string $PART_C_AREA4_4_4_6_MGT_RECORD
 * @property string $PART_C_AREA4_4_4_7_ORIENTATED
 * @property string $PART_C_AREA4_4_4_8_SUPPORT
 * @property string $PART_C_AREA4_4_4_9_COUNSELLING
 * @property string $PART_C_AREA4_4_4_10_SPIRITUAL
 * @property string $PART_C_AREA4_4_5_1_ORGANIZED
 * @property string $PART_C_AREA4_4_5_2_ACTIVELY
 * @property string $PART_C_AREA4_4_5_3_SKILLS
 * @property string $PART_C_AREA4_4_5_4_PUBLICATION
 * @property string $PART_C_AREA4_4_6_1_ALUMNI
 * @property string $PART_C_AREA4_4_6_2_FUTURE
 * @property string $PART_C_AREA4_4_6_3_ROLE_ALMNI
 * @property string $PART_C_AREA5_INDICATOR
 * @property string $PART_C_AREA5_5_1_1_RECRUITMENT
 * @property string $PART_C_AREA5_5_1_2_TERMS
 * @property string $PART_C_AREA5_5_1_3_MIN_QUALFCT
 * @property string $PART_C_AREA5_5_1_4_BASIS
 * @property string $PART_C_AREA5_5_1_5_PROFILE
 * @property string $PART_C_AREA5_5_1_6_STAFFING
 * @property string $PART_C_AREA5_5_1_7_CURR_VITAE
 * @property string $PART_C_AREA5_5_1_8_PERFORMANCE
 * @property string $PART_C_AREA5_5_1_9_PROCEDURES
 * @property string $PART_C_AREA5_5_1_10_POLICIES
 * @property string $PART_C_AREA5_5_1_11_STAFF_INFO
 * @property string $PART_C_AREA5_5_1_12_RECRUITMNT
 * @property string $PART_C_AREA5_5_1_13_RECRUITMNT
 * @property string $PART_C_AREA5_5_2_1_DEPARTMNTAL
 * @property string $PART_C_AREA5_5_2_2_TRAINING
 * @property string $PART_C_AREA5_5_2_3_MECHANISM
 * @property string $PART_C_AREA5_5_2_4_EXPERTISE
 * @property string $PART_C_AREA5_5_2_5_RESEARCH
 * @property string $PART_C_AREA5_5_2_6_PROFESSIONL
 * @property string $PART_C_AREA5_5_2_7_POLICY
 * @property string $PART_C_AREA5_5_2_8_MENTORING
 * @property string $PART_C_AREA5_5_2_9_COMMUNITY
 * @property string $PART_C_AREA5_5_2_10_EVIDENCE
 * @property string $PART_C_AREA5_5_2_11_OPPORTNITY
 * @property string $PART_C_AREA5_5_2_12_RSERCH_ACT
 * @property string $PART_C_AREA5_5_2_13_PROVISIONS
 * @property string $PART_C_AREA6_INDICATOR
 * @property string $PART_C_AREA6_6_1_1_PHYSCAL
 * @property string $PART_C_AREA6_6_1_2_ADEQUACY
 * @property string $PART_C_AREA6_6_1_3_UNMEET
 * @property string $PART_C_AREA6_6_1_4_CLINICAL
 * @property string $PART_C_AREA6_6_1_5_DEMOSTRATE
 * @property string $PART_C_AREA6_6_1_6_DATABASE
 * @property string $PART_C_AREA6_6_1_7_NO_STAFF
 * @property string $PART_C_AREA6_6_1_8_RESOURCE
 * @property string $PART_C_AREA6_6_1_9_REFERENCE
 * @property string $PART_C_AREA6_6_1_10_FEEDBACK
 * @property string $PART_C_AREA6_6_1_11_USE_ICT
 * @property string $PART_C_AREA6_6_1_12_ICT_STAFF
 * @property string $PART_C_AREA6_6_1_13_ICT_RQUIRE
 * @property string $PART_C_AREA6_6_1_14_PLANS
 * @property string $PART_C_AREA6_6_1_15_REVIEWS
 * @property string $PART_C_AREA6_6_1_16_OPPRTNTIES
 * @property string $PART_C_AREA6_6_1_17_SPECIAL
 * @property string $PART_C_AREA6_6_2_1_BUDGET
 * @property string $PART_C_AREA6_6_2_2_MAJOR
 * @property string $PART_C_AREA6_6_2_3_INTERACTION
 * @property string $PART_C_AREA6_6_2_4_INITIATIVES
 * @property string $PART_C_AREA6_6_2_5_LINK
 * @property string $PART_C_AREA6_6_2_6_REVIEWS
 * @property string $PART_C_AREA6_6_3_1_POLICY
 * @property string $PART_C_AREA6_6_3_2_ASSESS
 * @property string $PART_C_AREA6_6_4_1_PRACTICE
 * @property string $PART_C_AREA6_6_4_2_DISSEMINATE
 * @property string $PART_C_AREA6_6_4_3_FUTURE
 * @property string $PART_C_AREA6_6_4_4_FINANCIAL
 * @property string $PART_C_AREA6_6_5_1_RSPONSBLITS
 * @property string $PART_C_AREA6_6_5_2_ALLOCTION
 * @property string $PART_C_AREA6_6_5_3_RESPONSIBLE
 * @property string $PART_C_AREA7_INDICATOR
 * @property string $PART_C_AREA7_7_1_1_EVALUATES
 * @property string $PART_C_AREA7_7_1_2_PROGRESSION
 * @property string $PART_C_AREA7_7_1_3_MONITORING
 * @property string $PART_C_AREA7_7_1_4_ACHIEVEMENT
 * @property string $PART_C_AREA7_7_1_5_UTILISES
 * @property string $PART_C_AREA7_7_1_6_STRUCTURE
 * @property string $PART_C_AREA7_7_1_7_RSPNSBILITS
 * @property string $PART_C_AREA7_7_1_8_SELF_REVIEW
 * @property string $PART_C_AREA7_7_1_9_MECHANISM
 * @property string $PART_C_AREA7_7_2_1_STAKEHOLDER
 * @property string $PART_C_AREA7_7_2_2_CONSDERTION
 * @property string $PART_C_AREA7_7_2_3_INFORMS
 * @property string $PART_C_AREA7_7_2_4_FEEDBACKS
 * @property string $PART_C_AREA7_7_2_5_PROFSSIONAL
 * @property string $PART_C_AREA8_INDICATOR
 * @property string $PART_C_AREA8_8_1_1_POLICIES
 * @property string $PART_C_AREA8_8_1_2_GOVERNANCE
 * @property string $PART_C_AREA8_8_1_3_FREQUENCY
 * @property string $PART_C_AREA8_8_1_4_EVIDENCE
 * @property string $PART_C_AREA8_8_1_5_AUTONOMY
 * @property string $PART_C_AREA8_8_1_6_COMMITTEE
 * @property string $PART_C_AREA8_8_1_7_RPRSNTATION
 * @property string $PART_C_AREA8_8_2_1_SELECTION
 * @property string $PART_C_AREA8_8_2_2_MGT_STRUCT
 * @property string $PART_C_AREA8_8_2_3_CRITERIA
 * @property string $PART_C_AREA8_8_2_4_RLATIONSHIP
 * @property string $PART_C_AREA8_8_2_5_PERFORMANCE
 * @property string $PART_C_AREA8_8_2_6_CONDUCTIVE
 * @property string $PART_C_AREA8_8_3_1_ADMIN_STAFF
 * @property string $PART_C_AREA8_8_3_2_NO_ADMIN
 * @property string $PART_C_AREA8_8_3_3_MIN_QUALIFC
 * @property string $PART_C_AREA8_8_3_4_DETAIL_STAF
 * @property string $PART_C_AREA8_8_3_5_MECHANISM
 * @property string $PART_C_AREA8_8_3_6_MANAGING
 * @property string $PART_C_AREA8_8_3_7_TRAINING
 * @property string $PART_C_AREA8_8_3_8_CONDUCT_REG
 * @property string $PART_C_AREA8_8_3_9_SCHEME
 * @property string $PART_C_AREA8_8_4_1_SECURE
 * @property string $PART_C_AREA8_8_4_2_PRIVACY
 * @property string $PART_C_AREA8_8_4_3_DEPT_REVIEW
 * @property string $PART_C_AREA9_INDICATOR
 * @property string $PART_C_AREA9_9_1_1_COMPLEMENTS
 * @property string $PART_C_AREA9_9_1_2_CONTRBUTION
 * @property string $PART_C_AREA9_9_1_3_POLICIES
 * @property string $PART_C_AREA9_9_1_4_FREQUENCY
 * @property string $PART_C_AREA9_9_1_5_RECENT_ACT
 * @property string $PART_C_AREA9_9_1_6_ROLE
 * @property string $PART_C_AREA9_9_1_7_STEPS
 * @property string $PART_C_AREA3_INDICATOR
 * @property string $PART_C_AREA3_3_1_1_ASSEMNT
 * @property string $PART_C_AREA3_3_1_2_CONSTENT
 * @property string $PART_C_AREA3_3_1_3_MONITORS
 * @property string $PART_C_AREA3_3_1_4_DEPRTMENT
 * @property string $PART_C_AREA3_3_1_5_LEARNING
 * @property string $PART_C_AREA3_3_2_1_TERMS
 * @property string $PART_C_AREA3_3_2_2_PRCTICAL
 * @property string $PART_C_AREA3_3_2_3_METHDLOGIES
 * @property string $PART_C_AREA3_3_2_4_MONITORS
 * @property string $PART_C_AREA3_3_2_5_REVIEW
 * @property string $PART_C_AREA3_3_2_6_EXT_ASSMNT
 * @property string $PART_C_AREA3_3_2_7_EXT_EXPRTIS
 * @property string $PART_C_AREA3_3_3_1_AUTHORITY
 * @property string $PART_C_AREA3_3_3_2_CREDIBILTY
 * @property string $PART_C_AREA3_3_3_4_CONFDENTIAL
 * @property string $PART_C_AREA3_3_3_5_PRFORMANCE
 * @property string $PART_C_AREA3_3_3_6_FEEDBACK
 * @property string $PART_C_AREA3_3_3_7_RECORDS
 * @property string $PART_C_AREA3_3_3_8_APPEAL
 * @property string $PART_C_AREA3_3_3_9_MCHANISM
 * @property string $PART_C_AREA3_3_3_3_COMMITTEES
 * @property string $PART_C_AREA1_1_2_5_COMPTENCIES
 * @property string $PART_C_AREA2_2_1_2_RELTIONSHP
 * @property string $PART_C_AREA2_2_2_3_EVIDENCE
 * @property string $PART_C_AREA2_2_3_2_OFFER
 * @property string $PART_C_AREA2_2_5_3_OPPRTNITIES
 * @property string $PART_C_AREA1_1_1_1_LO
 * @property string $PART_C_AREA1_1_1_1_OBJ
 * @property string $PART_C_AREA1_1_2_1_LEARNING
 * @property string $PART_C_AREA1_1_2_2_MAP
 * @property string $PART_B_6
 * @property string $PART_C_AREA2_2_3
 */
class MQAProposal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EC_MQA_PROPOSAL';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ACAD_ID', 'PART_B_2', 'PART_B_4', 'PART_B_5', 'PART_B_7', 'PART_B_9', 'PART_C_AREA1_INDICATOR', 'PART_C_AREA2_INDICATOR', 'PART_C_AREA4_INDICATOR', 'PART_C_AREA5_INDICATOR', 'PART_C_AREA6_INDICATOR', 'PART_C_AREA7_INDICATOR', 'PART_C_AREA8_INDICATOR', 'PART_C_AREA9_INDICATOR', 'PART_C_AREA3_INDICATOR', 'PART_B_6'], 'string'],
            [['PART_B_1', 'PART_B_3', 'PART_B_8', 'PART_B_10', 'PART_B_11', 'PART_B_12', 'PART_B_13', 'PART_B_14', 'PART_B_15', 'PART_B_16', 'PART_B_17', 'PART_B_18', 'PART_B_19', 'PART_C_AREA1_1_1_1_AIM', 'PART_C_AREA1_1_1_1_OTHERS', 'PART_C_AREA1_1_1_2_MISSION', 'PART_C_AREA1_1_1_2_SHOWHOW', 'PART_C_AREA1_1_1_3_JUSTFICTION', 'PART_C_AREA1_1_1_3_MARKETNEED', 'PART_C_AREA1_1_1_3_RELATE', 'PART_C_AREA1_1_1_4_INCORPORATE', 'PART_C_AREA1_1_1_5_CONSULTED', 'PART_C_AREA1_1_2_3_ACHIEVEMENT', 'PART_C_AREA1_1_2_4_LEARNING', 'PART_C_AREA2_2_1_1_PROVISIONS', 'PART_C_AREA2_2_1_3_AUTONOMY', 'PART_C_AREA2_2_1_4_POLICIES', 'PART_C_AREA2_2_1_5_HEP_PLANS', 'PART_C_AREA2_2_1_5_DEPT_ROLE', 'PART_C_AREA2_2_2_1_PROCESS', 'PART_C_AREA2_2_2_2_TEACHGMTHOD', 'PART_C_AREA2_2_2_4_INQUIRY', 'PART_C_AREA2_2_2_5_LEARNGMTHOD', 'PART_C_AREA2_2_2_6_MULTI', 'PART_C_AREA2_2_2_7_EXTERNAL', 'PART_C_AREA2_2_2_8_ACTIVITIES', 'PART_C_AREA2_2_3_1_SUBJECTS', 'PART_C_AREA2_2_3_3_COURSE', 'PART_C_AREA2_2_3_4_PLAN', 'PART_C_AREA2_2_3_5_EVIDENCE', 'PART_C_AREA2_2_4_1_SAMPLE', 'PART_C_AREA2_2_4_2_MANNER', 'PART_C_AREA2_2_4_3_DESGNATION', 'PART_C_AREA2_2_4_4_REVIEW', 'PART_C_AREA2_2_4_5_LEARNING', 'PART_C_AREA2_2_4_6_INITIATIVE', 'PART_C_AREA2_2_4_7_ENGAGES', 'PART_C_AREA2_2_5_1_LINKS', 'PART_C_AREA2_2_5_2_MECHNISM', 'PART_C_AREA3_3_3_10_COPY', 'PART_C_AREA3_3_3_11_IMPROVING', 'PART_C_AREA3_3_3_12_AUTONMIES', 'PART_C_AREA3_3_3_13_NATURE', 'PART_C_AREA4_4_1_1_RESPONSBLE', 'PART_C_AREA4_4_1_2_EVIDENCE', 'PART_C_AREA4_4_1_3_ADMISSION', 'PART_C_AREA4_4_1_4_CRITERIA', 'PART_C_AREA4_4_1_5_APPEAL', 'PART_C_AREA4_4_1_6_CHRCTERSTCS', 'PART_C_AREA4_4_1_7_FORECAST', 'PART_C_AREA4_4_1_8_SELECTION', 'PART_C_AREA4_4_1_9_INTERVIEW', 'PART_C_AREA4_4_1_10_SPECIAL', 'PART_C_AREA4_4_1_11_MONITOR', 'PART_C_AREA4_4_1_12_ENGAGE', 'PART_C_AREA4_4_1_13_RELTIONSHP', 'PART_C_AREA4_4_2_1_POLICIES', 'PART_C_AREA4_4_2_2_DVLOPMENT', 'PART_C_AREA4_4_3_1_POLICY', 'PART_C_AREA4_4_3_2_ACCEPTED', 'PART_C_AREA4_4_3_3_FACILITIES', 'PART_C_AREA4_4_4_1_SUPPORT', 'PART_C_AREA4_4_4_2_ACCESSBLITY', 'PART_C_AREA4_4_4_3_COMPLAIN', 'PART_C_AREA4_4_4_4_ADEQUACY', 'PART_C_AREA4_4_4_5_ROLES', 'PART_C_AREA4_4_4_6_MGT_RECORD', 'PART_C_AREA4_4_4_7_ORIENTATED', 'PART_C_AREA4_4_4_8_SUPPORT', 'PART_C_AREA4_4_4_9_COUNSELLING', 'PART_C_AREA4_4_4_10_SPIRITUAL', 'PART_C_AREA4_4_5_1_ORGANIZED', 'PART_C_AREA4_4_5_2_ACTIVELY', 'PART_C_AREA4_4_5_3_SKILLS', 'PART_C_AREA4_4_5_4_PUBLICATION', 'PART_C_AREA4_4_6_1_ALUMNI', 'PART_C_AREA4_4_6_2_FUTURE', 'PART_C_AREA4_4_6_3_ROLE_ALMNI', 'PART_C_AREA5_5_1_1_RECRUITMENT', 'PART_C_AREA5_5_1_2_TERMS', 'PART_C_AREA5_5_1_3_MIN_QUALFCT', 'PART_C_AREA5_5_1_4_BASIS', 'PART_C_AREA5_5_1_5_PROFILE', 'PART_C_AREA5_5_1_6_STAFFING', 'PART_C_AREA5_5_1_7_CURR_VITAE', 'PART_C_AREA5_5_1_8_PERFORMANCE', 'PART_C_AREA5_5_1_9_PROCEDURES', 'PART_C_AREA5_5_1_10_POLICIES', 'PART_C_AREA5_5_1_11_STAFF_INFO', 'PART_C_AREA5_5_1_12_RECRUITMNT', 'PART_C_AREA5_5_1_13_RECRUITMNT', 'PART_C_AREA5_5_2_1_DEPARTMNTAL', 'PART_C_AREA5_5_2_2_TRAINING', 'PART_C_AREA5_5_2_3_MECHANISM', 'PART_C_AREA5_5_2_4_EXPERTISE', 'PART_C_AREA5_5_2_5_RESEARCH', 'PART_C_AREA5_5_2_6_PROFESSIONL', 'PART_C_AREA5_5_2_7_POLICY', 'PART_C_AREA5_5_2_8_MENTORING', 'PART_C_AREA5_5_2_9_COMMUNITY', 'PART_C_AREA5_5_2_10_EVIDENCE', 'PART_C_AREA5_5_2_11_OPPORTNITY', 'PART_C_AREA5_5_2_12_RSERCH_ACT', 'PART_C_AREA5_5_2_13_PROVISIONS', 'PART_C_AREA6_6_1_1_PHYSCAL', 'PART_C_AREA6_6_1_2_ADEQUACY', 'PART_C_AREA6_6_1_3_UNMEET', 'PART_C_AREA6_6_1_4_CLINICAL', 'PART_C_AREA6_6_1_5_DEMOSTRATE', 'PART_C_AREA6_6_1_6_DATABASE', 'PART_C_AREA6_6_1_7_NO_STAFF', 'PART_C_AREA6_6_1_8_RESOURCE', 'PART_C_AREA6_6_1_9_REFERENCE', 'PART_C_AREA6_6_1_10_FEEDBACK', 'PART_C_AREA6_6_1_11_USE_ICT', 'PART_C_AREA6_6_1_12_ICT_STAFF', 'PART_C_AREA6_6_1_13_ICT_RQUIRE', 'PART_C_AREA6_6_1_14_PLANS', 'PART_C_AREA6_6_1_15_REVIEWS', 'PART_C_AREA6_6_1_16_OPPRTNTIES', 'PART_C_AREA6_6_1_17_SPECIAL', 'PART_C_AREA6_6_2_1_BUDGET', 'PART_C_AREA6_6_2_2_MAJOR', 'PART_C_AREA6_6_2_3_INTERACTION', 'PART_C_AREA6_6_2_4_INITIATIVES', 'PART_C_AREA6_6_2_5_LINK', 'PART_C_AREA6_6_2_6_REVIEWS', 'PART_C_AREA6_6_3_1_POLICY', 'PART_C_AREA6_6_3_2_ASSESS', 'PART_C_AREA6_6_4_1_PRACTICE', 'PART_C_AREA6_6_4_2_DISSEMINATE', 'PART_C_AREA6_6_4_3_FUTURE', 'PART_C_AREA6_6_4_4_FINANCIAL', 'PART_C_AREA6_6_5_1_RSPONSBLITS', 'PART_C_AREA6_6_5_2_ALLOCTION', 'PART_C_AREA6_6_5_3_RESPONSIBLE', 'PART_C_AREA7_7_1_1_EVALUATES', 'PART_C_AREA7_7_1_2_PROGRESSION', 'PART_C_AREA7_7_1_3_MONITORING', 'PART_C_AREA7_7_1_4_ACHIEVEMENT', 'PART_C_AREA7_7_1_5_UTILISES', 'PART_C_AREA7_7_1_6_STRUCTURE', 'PART_C_AREA7_7_1_7_RSPNSBILITS', 'PART_C_AREA7_7_1_8_SELF_REVIEW', 'PART_C_AREA7_7_1_9_MECHANISM', 'PART_C_AREA7_7_2_1_STAKEHOLDER', 'PART_C_AREA7_7_2_2_CONSDERTION', 'PART_C_AREA7_7_2_3_INFORMS', 'PART_C_AREA7_7_2_4_FEEDBACKS', 'PART_C_AREA7_7_2_5_PROFSSIONAL', 'PART_C_AREA8_8_1_1_POLICIES', 'PART_C_AREA8_8_1_2_GOVERNANCE', 'PART_C_AREA8_8_1_3_FREQUENCY', 'PART_C_AREA8_8_1_4_EVIDENCE', 'PART_C_AREA8_8_1_5_AUTONOMY', 'PART_C_AREA8_8_1_6_COMMITTEE', 'PART_C_AREA8_8_1_7_RPRSNTATION', 'PART_C_AREA8_8_2_1_SELECTION', 'PART_C_AREA8_8_2_2_MGT_STRUCT', 'PART_C_AREA8_8_2_3_CRITERIA', 'PART_C_AREA8_8_2_4_RLATIONSHIP', 'PART_C_AREA8_8_2_5_PERFORMANCE', 'PART_C_AREA8_8_2_6_CONDUCTIVE', 'PART_C_AREA8_8_3_1_ADMIN_STAFF', 'PART_C_AREA8_8_3_2_NO_ADMIN', 'PART_C_AREA8_8_3_3_MIN_QUALIFC', 'PART_C_AREA8_8_3_4_DETAIL_STAF', 'PART_C_AREA8_8_3_5_MECHANISM', 'PART_C_AREA8_8_3_6_MANAGING', 'PART_C_AREA8_8_3_7_TRAINING', 'PART_C_AREA8_8_3_8_CONDUCT_REG', 'PART_C_AREA8_8_3_9_SCHEME', 'PART_C_AREA8_8_4_1_SECURE', 'PART_C_AREA8_8_4_2_PRIVACY', 'PART_C_AREA8_8_4_3_DEPT_REVIEW', 'PART_C_AREA9_9_1_1_COMPLEMENTS', 'PART_C_AREA9_9_1_2_CONTRBUTION', 'PART_C_AREA9_9_1_3_POLICIES', 'PART_C_AREA9_9_1_4_FREQUENCY', 'PART_C_AREA9_9_1_5_RECENT_ACT', 'PART_C_AREA9_9_1_6_ROLE', 'PART_C_AREA9_9_1_7_STEPS', 'PART_C_AREA3_3_1_1_ASSEMNT', 'PART_C_AREA3_3_1_2_CONSTENT', 'PART_C_AREA3_3_1_3_MONITORS', 'PART_C_AREA3_3_1_4_DEPRTMENT', 'PART_C_AREA3_3_1_5_LEARNING', 'PART_C_AREA3_3_2_1_TERMS', 'PART_C_AREA3_3_2_2_PRCTICAL', 'PART_C_AREA3_3_2_3_METHDLOGIES', 'PART_C_AREA3_3_2_4_MONITORS', 'PART_C_AREA3_3_2_5_REVIEW', 'PART_C_AREA3_3_2_6_EXT_ASSMNT', 'PART_C_AREA3_3_2_7_EXT_EXPRTIS', 'PART_C_AREA3_3_3_1_AUTHORITY', 'PART_C_AREA3_3_3_2_CREDIBILTY', 'PART_C_AREA3_3_3_4_CONFDENTIAL', 'PART_C_AREA3_3_3_5_PRFORMANCE', 'PART_C_AREA3_3_3_6_FEEDBACK', 'PART_C_AREA3_3_3_7_RECORDS', 'PART_C_AREA3_3_3_8_APPEAL', 'PART_C_AREA3_3_3_9_MCHANISM', 'PART_C_AREA3_3_3_3_COMMITTEES', 'PART_C_AREA1_1_2_5_COMPTENCIES', 'PART_C_AREA2_2_1_2_RELTIONSHP', 'PART_C_AREA2_2_2_3_EVIDENCE', 'PART_C_AREA2_2_3_2_OFFER', 'PART_C_AREA2_2_5_3_OPPRTNITIES', 'PART_C_AREA1_1_1_1_LO', 'PART_C_AREA1_1_1_1_OBJ', 'PART_C_AREA1_1_2_1_LEARNING', 'PART_C_AREA1_1_2_2_MAP', 'PART_C_AREA2_2_3'], 'string', 'max' => 4000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'MQA_PROPOSAL_ID' => Yii::t('app', 'Mqa  Proposal  ID'),
            'ACAD_ID' => Yii::t('app', 'Acad  ID'),
            'PART_B_1' => Yii::t('app', 'Part  B 1'),
            'PART_B_3' => Yii::t('app', 'Part  B 3'),
            'PART_B_8' => Yii::t('app', 'Part  B 8'),
            'PART_B_10' => Yii::t('app', 'Part  B 10'),
            'PART_B_11' => Yii::t('app', 'Part  B 11'),
            'PART_B_12' => Yii::t('app', 'Part  B 12'),
            'PART_B_13' => Yii::t('app', 'Part  B 13'),
            'PART_B_14' => Yii::t('app', 'Part  B 14'),
            'PART_B_15' => Yii::t('app', 'Part  B 15'),
            'PART_B_16' => Yii::t('app', 'Part  B 16'),
            'PART_B_17' => Yii::t('app', 'Part  B 17'),
            'PART_B_18' => Yii::t('app', 'Part  B 18'),
            'PART_B_19' => Yii::t('app', 'Part  B 19'),
            'PART_C_AREA1_1_1_1_AIM' => Yii::t('app', 'Part  C  Area1 1 1 1  Aim'),
            'PART_C_AREA1_1_1_1_OTHERS' => Yii::t('app', 'Part  C  Area1 1 1 1  Others'),
            'PART_C_AREA1_1_1_2_MISSION' => Yii::t('app', 'Part  C  Area1 1 1 2  Mission'),
            'PART_C_AREA1_1_1_2_SHOWHOW' => Yii::t('app', 'Part  C  Area1 1 1 2  Showhow'),
            'PART_C_AREA1_1_1_3_JUSTFICTION' => Yii::t('app', 'Part  C  Area1 1 1 3  Justfiction'),
            'PART_C_AREA1_1_1_3_MARKETNEED' => Yii::t('app', 'Part  C  Area1 1 1 3  Marketneed'),
            'PART_C_AREA1_1_1_3_RELATE' => Yii::t('app', 'Part  C  Area1 1 1 3  Relate'),
            'PART_C_AREA1_1_1_4_INCORPORATE' => Yii::t('app', 'Part  C  Area1 1 1 4  Incorporate'),
            'PART_B_2' => Yii::t('app', 'Part  B 2'),
            'PART_B_4' => Yii::t('app', 'Part  B 4'),
            'PART_B_5' => Yii::t('app', 'Part  B 5'),
            'PART_B_7' => Yii::t('app', 'Part  B 7'),
            'PART_B_9' => Yii::t('app', 'Part  B 9'),
            'PART_C_AREA1_INDICATOR' => Yii::t('app', 'Part  C  Area1  Indicator'),
            'PART_C_AREA1_1_1_5_CONSULTED' => Yii::t('app', 'Part  C  Area1 1 1 5  Consulted'),
            'PART_C_AREA1_1_2_3_ACHIEVEMENT' => Yii::t('app', 'Part  C  Area1 1 2 3  Achievement'),
            'PART_C_AREA1_1_2_4_LEARNING' => Yii::t('app', 'Part  C  Area1 1 2 4  Learning'),
            'PART_C_AREA2_INDICATOR' => Yii::t('app', 'Part  C  Area2  Indicator'),
            'PART_C_AREA2_2_1_1_PROVISIONS' => Yii::t('app', 'Part  C  Area2 2 1 1  Provisions'),
            'PART_C_AREA2_2_1_3_AUTONOMY' => Yii::t('app', 'Part  C  Area2 2 1 3  Autonomy'),
            'PART_C_AREA2_2_1_4_POLICIES' => Yii::t('app', 'Part  C  Area2 2 1 4  Policies'),
            'PART_C_AREA2_2_1_5_HEP_PLANS' => Yii::t('app', 'Part  C  Area2 2 1 5  Hep  Plans'),
            'PART_C_AREA2_2_1_5_DEPT_ROLE' => Yii::t('app', 'Part  C  Area2 2 1 5  Dept  Role'),
            'PART_C_AREA2_2_2_1_PROCESS' => Yii::t('app', 'Part  C  Area2 2 2 1  Process'),
            'PART_C_AREA2_2_2_2_TEACHGMTHOD' => Yii::t('app', 'Part  C  Area2 2 2 2  Teachgmthod'),
            'PART_C_AREA2_2_2_4_INQUIRY' => Yii::t('app', 'Part  C  Area2 2 2 4  Inquiry'),
            'PART_C_AREA2_2_2_5_LEARNGMTHOD' => Yii::t('app', 'Part  C  Area2 2 2 5  Learngmthod'),
            'PART_C_AREA2_2_2_6_MULTI' => Yii::t('app', 'Part  C  Area2 2 2 6  Multi'),
            'PART_C_AREA2_2_2_7_EXTERNAL' => Yii::t('app', 'Part  C  Area2 2 2 7  External'),
            'PART_C_AREA2_2_2_8_ACTIVITIES' => Yii::t('app', 'Part  C  Area2 2 2 8  Activities'),
            'PART_C_AREA2_2_3_1_SUBJECTS' => Yii::t('app', 'Part  C  Area2 2 3 1  Subjects'),
            'PART_C_AREA2_2_3_3_COURSE' => Yii::t('app', 'Part  C  Area2 2 3 3  Course'),
            'PART_C_AREA2_2_3_4_PLAN' => Yii::t('app', 'Part  C  Area2 2 3 4  Plan'),
            'PART_C_AREA2_2_3_5_EVIDENCE' => Yii::t('app', 'Part  C  Area2 2 3 5  Evidence'),
            'PART_C_AREA2_2_4_1_SAMPLE' => Yii::t('app', 'Part  C  Area2 2 4 1  Sample'),
            'PART_C_AREA2_2_4_2_MANNER' => Yii::t('app', 'Part  C  Area2 2 4 2  Manner'),
            'PART_C_AREA2_2_4_3_DESGNATION' => Yii::t('app', 'Part  C  Area2 2 4 3  Desgnation'),
            'PART_C_AREA2_2_4_4_REVIEW' => Yii::t('app', 'Part  C  Area2 2 4 4  Review'),
            'PART_C_AREA2_2_4_5_LEARNING' => Yii::t('app', 'Part  C  Area2 2 4 5  Learning'),
            'PART_C_AREA2_2_4_6_INITIATIVE' => Yii::t('app', 'Part  C  Area2 2 4 6  Initiative'),
            'PART_C_AREA2_2_4_7_ENGAGES' => Yii::t('app', 'Part  C  Area2 2 4 7  Engages'),
            'PART_C_AREA2_2_5_1_LINKS' => Yii::t('app', 'Part  C  Area2 2 5 1  Links'),
            'PART_C_AREA2_2_5_2_MECHNISM' => Yii::t('app', 'Part  C  Area2 2 5 2  Mechnism'),
            'PART_C_AREA3_3_3_10_COPY' => Yii::t('app', 'Part  C  Area3 3 3 10  Copy'),
            'PART_C_AREA3_3_3_11_IMPROVING' => Yii::t('app', 'Part  C  Area3 3 3 11  Improving'),
            'PART_C_AREA3_3_3_12_AUTONMIES' => Yii::t('app', 'Part  C  Area3 3 3 12  Autonmies'),
            'PART_C_AREA3_3_3_13_NATURE' => Yii::t('app', 'Part  C  Area3 3 3 13  Nature'),
            'PART_C_AREA4_INDICATOR' => Yii::t('app', 'Part  C  Area4  Indicator'),
            'PART_C_AREA4_4_1_1_RESPONSBLE' => Yii::t('app', 'Part  C  Area4 4 1 1  Responsble'),
            'PART_C_AREA4_4_1_2_EVIDENCE' => Yii::t('app', 'Part  C  Area4 4 1 2  Evidence'),
            'PART_C_AREA4_4_1_3_ADMISSION' => Yii::t('app', 'Part  C  Area4 4 1 3  Admission'),
            'PART_C_AREA4_4_1_4_CRITERIA' => Yii::t('app', 'Part  C  Area4 4 1 4  Criteria'),
            'PART_C_AREA4_4_1_5_APPEAL' => Yii::t('app', 'Part  C  Area4 4 1 5  Appeal'),
            'PART_C_AREA4_4_1_6_CHRCTERSTCS' => Yii::t('app', 'Part  C  Area4 4 1 6  Chrcterstcs'),
            'PART_C_AREA4_4_1_7_FORECAST' => Yii::t('app', 'Part  C  Area4 4 1 7  Forecast'),
            'PART_C_AREA4_4_1_8_SELECTION' => Yii::t('app', 'Part  C  Area4 4 1 8  Selection'),
            'PART_C_AREA4_4_1_9_INTERVIEW' => Yii::t('app', 'Part  C  Area4 4 1 9  Interview'),
            'PART_C_AREA4_4_1_10_SPECIAL' => Yii::t('app', 'Part  C  Area4 4 1 10  Special'),
            'PART_C_AREA4_4_1_11_MONITOR' => Yii::t('app', 'Part  C  Area4 4 1 11  Monitor'),
            'PART_C_AREA4_4_1_12_ENGAGE' => Yii::t('app', 'Part  C  Area4 4 1 12  Engage'),
            'PART_C_AREA4_4_1_13_RELTIONSHP' => Yii::t('app', 'Part  C  Area4 4 1 13  Reltionshp'),
            'PART_C_AREA4_4_2_1_POLICIES' => Yii::t('app', 'Part  C  Area4 4 2 1  Policies'),
            'PART_C_AREA4_4_2_2_DVLOPMENT' => Yii::t('app', 'Part  C  Area4 4 2 2  Dvlopment'),
            'PART_C_AREA4_4_3_1_POLICY' => Yii::t('app', 'Part  C  Area4 4 3 1  Policy'),
            'PART_C_AREA4_4_3_2_ACCEPTED' => Yii::t('app', 'Part  C  Area4 4 3 2  Accepted'),
            'PART_C_AREA4_4_3_3_FACILITIES' => Yii::t('app', 'Part  C  Area4 4 3 3  Facilities'),
            'PART_C_AREA4_4_4_1_SUPPORT' => Yii::t('app', 'Part  C  Area4 4 4 1  Support'),
            'PART_C_AREA4_4_4_2_ACCESSBLITY' => Yii::t('app', 'Part  C  Area4 4 4 2  Accessblity'),
            'PART_C_AREA4_4_4_3_COMPLAIN' => Yii::t('app', 'Part  C  Area4 4 4 3  Complain'),
            'PART_C_AREA4_4_4_4_ADEQUACY' => Yii::t('app', 'Part  C  Area4 4 4 4  Adequacy'),
            'PART_C_AREA4_4_4_5_ROLES' => Yii::t('app', 'Part  C  Area4 4 4 5  Roles'),
            'PART_C_AREA4_4_4_6_MGT_RECORD' => Yii::t('app', 'Part  C  Area4 4 4 6  Mgt  Record'),
            'PART_C_AREA4_4_4_7_ORIENTATED' => Yii::t('app', 'Part  C  Area4 4 4 7  Orientated'),
            'PART_C_AREA4_4_4_8_SUPPORT' => Yii::t('app', 'Part  C  Area4 4 4 8  Support'),
            'PART_C_AREA4_4_4_9_COUNSELLING' => Yii::t('app', 'Part  C  Area4 4 4 9  Counselling'),
            'PART_C_AREA4_4_4_10_SPIRITUAL' => Yii::t('app', 'Part  C  Area4 4 4 10  Spiritual'),
            'PART_C_AREA4_4_5_1_ORGANIZED' => Yii::t('app', 'Part  C  Area4 4 5 1  Organized'),
            'PART_C_AREA4_4_5_2_ACTIVELY' => Yii::t('app', 'Part  C  Area4 4 5 2  Actively'),
            'PART_C_AREA4_4_5_3_SKILLS' => Yii::t('app', 'Part  C  Area4 4 5 3  Skills'),
            'PART_C_AREA4_4_5_4_PUBLICATION' => Yii::t('app', 'Part  C  Area4 4 5 4  Publication'),
            'PART_C_AREA4_4_6_1_ALUMNI' => Yii::t('app', 'Part  C  Area4 4 6 1  Alumni'),
            'PART_C_AREA4_4_6_2_FUTURE' => Yii::t('app', 'Part  C  Area4 4 6 2  Future'),
            'PART_C_AREA4_4_6_3_ROLE_ALMNI' => Yii::t('app', 'Part  C  Area4 4 6 3  Role  Almni'),
            'PART_C_AREA5_INDICATOR' => Yii::t('app', 'Part  C  Area5  Indicator'),
            'PART_C_AREA5_5_1_1_RECRUITMENT' => Yii::t('app', 'Part  C  Area5 5 1 1  Recruitment'),
            'PART_C_AREA5_5_1_2_TERMS' => Yii::t('app', 'Part  C  Area5 5 1 2  Terms'),
            'PART_C_AREA5_5_1_3_MIN_QUALFCT' => Yii::t('app', 'Part  C  Area5 5 1 3  Min  Qualfct'),
            'PART_C_AREA5_5_1_4_BASIS' => Yii::t('app', 'Part  C  Area5 5 1 4  Basis'),
            'PART_C_AREA5_5_1_5_PROFILE' => Yii::t('app', 'Part  C  Area5 5 1 5  Profile'),
            'PART_C_AREA5_5_1_6_STAFFING' => Yii::t('app', 'Part  C  Area5 5 1 6  Staffing'),
            'PART_C_AREA5_5_1_7_CURR_VITAE' => Yii::t('app', 'Part  C  Area5 5 1 7  Curr  Vitae'),
            'PART_C_AREA5_5_1_8_PERFORMANCE' => Yii::t('app', 'Part  C  Area5 5 1 8  Performance'),
            'PART_C_AREA5_5_1_9_PROCEDURES' => Yii::t('app', 'Part  C  Area5 5 1 9  Procedures'),
            'PART_C_AREA5_5_1_10_POLICIES' => Yii::t('app', 'Part  C  Area5 5 1 10  Policies'),
            'PART_C_AREA5_5_1_11_STAFF_INFO' => Yii::t('app', 'Part  C  Area5 5 1 11  Staff  Info'),
            'PART_C_AREA5_5_1_12_RECRUITMNT' => Yii::t('app', 'Part  C  Area5 5 1 12  Recruitmnt'),
            'PART_C_AREA5_5_1_13_RECRUITMNT' => Yii::t('app', 'Part  C  Area5 5 1 13  Recruitmnt'),
            'PART_C_AREA5_5_2_1_DEPARTMNTAL' => Yii::t('app', 'Part  C  Area5 5 2 1  Departmntal'),
            'PART_C_AREA5_5_2_2_TRAINING' => Yii::t('app', 'Part  C  Area5 5 2 2  Training'),
            'PART_C_AREA5_5_2_3_MECHANISM' => Yii::t('app', 'Part  C  Area5 5 2 3  Mechanism'),
            'PART_C_AREA5_5_2_4_EXPERTISE' => Yii::t('app', 'Part  C  Area5 5 2 4  Expertise'),
            'PART_C_AREA5_5_2_5_RESEARCH' => Yii::t('app', 'Part  C  Area5 5 2 5  Research'),
            'PART_C_AREA5_5_2_6_PROFESSIONL' => Yii::t('app', 'Part  C  Area5 5 2 6  Professionl'),
            'PART_C_AREA5_5_2_7_POLICY' => Yii::t('app', 'Part  C  Area5 5 2 7  Policy'),
            'PART_C_AREA5_5_2_8_MENTORING' => Yii::t('app', 'Part  C  Area5 5 2 8  Mentoring'),
            'PART_C_AREA5_5_2_9_COMMUNITY' => Yii::t('app', 'Part  C  Area5 5 2 9  Community'),
            'PART_C_AREA5_5_2_10_EVIDENCE' => Yii::t('app', 'Part  C  Area5 5 2 10  Evidence'),
            'PART_C_AREA5_5_2_11_OPPORTNITY' => Yii::t('app', 'Part  C  Area5 5 2 11  Opportnity'),
            'PART_C_AREA5_5_2_12_RSERCH_ACT' => Yii::t('app', 'Part  C  Area5 5 2 12  Rserch  Act'),
            'PART_C_AREA5_5_2_13_PROVISIONS' => Yii::t('app', 'Part  C  Area5 5 2 13  Provisions'),
            'PART_C_AREA6_INDICATOR' => Yii::t('app', 'Part  C  Area6  Indicator'),
            'PART_C_AREA6_6_1_1_PHYSCAL' => Yii::t('app', 'Part  C  Area6 6 1 1  Physcal'),
            'PART_C_AREA6_6_1_2_ADEQUACY' => Yii::t('app', 'Part  C  Area6 6 1 2  Adequacy'),
            'PART_C_AREA6_6_1_3_UNMEET' => Yii::t('app', 'Part  C  Area6 6 1 3  Unmeet'),
            'PART_C_AREA6_6_1_4_CLINICAL' => Yii::t('app', 'Part  C  Area6 6 1 4  Clinical'),
            'PART_C_AREA6_6_1_5_DEMOSTRATE' => Yii::t('app', 'Part  C  Area6 6 1 5  Demostrate'),
            'PART_C_AREA6_6_1_6_DATABASE' => Yii::t('app', 'Part  C  Area6 6 1 6  Database'),
            'PART_C_AREA6_6_1_7_NO_STAFF' => Yii::t('app', 'Part  C  Area6 6 1 7  No  Staff'),
            'PART_C_AREA6_6_1_8_RESOURCE' => Yii::t('app', 'Part  C  Area6 6 1 8  Resource'),
            'PART_C_AREA6_6_1_9_REFERENCE' => Yii::t('app', 'Part  C  Area6 6 1 9  Reference'),
            'PART_C_AREA6_6_1_10_FEEDBACK' => Yii::t('app', 'Part  C  Area6 6 1 10  Feedback'),
            'PART_C_AREA6_6_1_11_USE_ICT' => Yii::t('app', 'Part  C  Area6 6 1 11  Use  Ict'),
            'PART_C_AREA6_6_1_12_ICT_STAFF' => Yii::t('app', 'Part  C  Area6 6 1 12  Ict  Staff'),
            'PART_C_AREA6_6_1_13_ICT_RQUIRE' => Yii::t('app', 'Part  C  Area6 6 1 13  Ict  Rquire'),
            'PART_C_AREA6_6_1_14_PLANS' => Yii::t('app', 'Part  C  Area6 6 1 14  Plans'),
            'PART_C_AREA6_6_1_15_REVIEWS' => Yii::t('app', 'Part  C  Area6 6 1 15  Reviews'),
            'PART_C_AREA6_6_1_16_OPPRTNTIES' => Yii::t('app', 'Part  C  Area6 6 1 16  Opprtnties'),
            'PART_C_AREA6_6_1_17_SPECIAL' => Yii::t('app', 'Part  C  Area6 6 1 17  Special'),
            'PART_C_AREA6_6_2_1_BUDGET' => Yii::t('app', 'Part  C  Area6 6 2 1  Budget'),
            'PART_C_AREA6_6_2_2_MAJOR' => Yii::t('app', 'Part  C  Area6 6 2 2  Major'),
            'PART_C_AREA6_6_2_3_INTERACTION' => Yii::t('app', 'Part  C  Area6 6 2 3  Interaction'),
            'PART_C_AREA6_6_2_4_INITIATIVES' => Yii::t('app', 'Part  C  Area6 6 2 4  Initiatives'),
            'PART_C_AREA6_6_2_5_LINK' => Yii::t('app', 'Part  C  Area6 6 2 5  Link'),
            'PART_C_AREA6_6_2_6_REVIEWS' => Yii::t('app', 'Part  C  Area6 6 2 6  Reviews'),
            'PART_C_AREA6_6_3_1_POLICY' => Yii::t('app', 'Part  C  Area6 6 3 1  Policy'),
            'PART_C_AREA6_6_3_2_ASSESS' => Yii::t('app', 'Part  C  Area6 6 3 2  Assess'),
            'PART_C_AREA6_6_4_1_PRACTICE' => Yii::t('app', 'Part  C  Area6 6 4 1  Practice'),
            'PART_C_AREA6_6_4_2_DISSEMINATE' => Yii::t('app', 'Part  C  Area6 6 4 2  Disseminate'),
            'PART_C_AREA6_6_4_3_FUTURE' => Yii::t('app', 'Part  C  Area6 6 4 3  Future'),
            'PART_C_AREA6_6_4_4_FINANCIAL' => Yii::t('app', 'Part  C  Area6 6 4 4  Financial'),
            'PART_C_AREA6_6_5_1_RSPONSBLITS' => Yii::t('app', 'Part  C  Area6 6 5 1  Rsponsblits'),
            'PART_C_AREA6_6_5_2_ALLOCTION' => Yii::t('app', 'Part  C  Area6 6 5 2  Alloction'),
            'PART_C_AREA6_6_5_3_RESPONSIBLE' => Yii::t('app', 'Part  C  Area6 6 5 3  Responsible'),
            'PART_C_AREA7_INDICATOR' => Yii::t('app', 'Part  C  Area7  Indicator'),
            'PART_C_AREA7_7_1_1_EVALUATES' => Yii::t('app', 'Part  C  Area7 7 1 1  Evaluates'),
            'PART_C_AREA7_7_1_2_PROGRESSION' => Yii::t('app', 'Part  C  Area7 7 1 2  Progression'),
            'PART_C_AREA7_7_1_3_MONITORING' => Yii::t('app', 'Part  C  Area7 7 1 3  Monitoring'),
            'PART_C_AREA7_7_1_4_ACHIEVEMENT' => Yii::t('app', 'Part  C  Area7 7 1 4  Achievement'),
            'PART_C_AREA7_7_1_5_UTILISES' => Yii::t('app', 'Part  C  Area7 7 1 5  Utilises'),
            'PART_C_AREA7_7_1_6_STRUCTURE' => Yii::t('app', 'Part  C  Area7 7 1 6  Structure'),
            'PART_C_AREA7_7_1_7_RSPNSBILITS' => Yii::t('app', 'Part  C  Area7 7 1 7  Rspnsbilits'),
            'PART_C_AREA7_7_1_8_SELF_REVIEW' => Yii::t('app', 'Part  C  Area7 7 1 8  Self  Review'),
            'PART_C_AREA7_7_1_9_MECHANISM' => Yii::t('app', 'Part  C  Area7 7 1 9  Mechanism'),
            'PART_C_AREA7_7_2_1_STAKEHOLDER' => Yii::t('app', 'Part  C  Area7 7 2 1  Stakeholder'),
            'PART_C_AREA7_7_2_2_CONSDERTION' => Yii::t('app', 'Part  C  Area7 7 2 2  Consdertion'),
            'PART_C_AREA7_7_2_3_INFORMS' => Yii::t('app', 'Part  C  Area7 7 2 3  Informs'),
            'PART_C_AREA7_7_2_4_FEEDBACKS' => Yii::t('app', 'Part  C  Area7 7 2 4  Feedbacks'),
            'PART_C_AREA7_7_2_5_PROFSSIONAL' => Yii::t('app', 'Part  C  Area7 7 2 5  Profssional'),
            'PART_C_AREA8_INDICATOR' => Yii::t('app', 'Part  C  Area8  Indicator'),
            'PART_C_AREA8_8_1_1_POLICIES' => Yii::t('app', 'Part  C  Area8 8 1 1  Policies'),
            'PART_C_AREA8_8_1_2_GOVERNANCE' => Yii::t('app', 'Part  C  Area8 8 1 2  Governance'),
            'PART_C_AREA8_8_1_3_FREQUENCY' => Yii::t('app', 'Part  C  Area8 8 1 3  Frequency'),
            'PART_C_AREA8_8_1_4_EVIDENCE' => Yii::t('app', 'Part  C  Area8 8 1 4  Evidence'),
            'PART_C_AREA8_8_1_5_AUTONOMY' => Yii::t('app', 'Part  C  Area8 8 1 5  Autonomy'),
            'PART_C_AREA8_8_1_6_COMMITTEE' => Yii::t('app', 'Part  C  Area8 8 1 6  Committee'),
            'PART_C_AREA8_8_1_7_RPRSNTATION' => Yii::t('app', 'Part  C  Area8 8 1 7  Rprsntation'),
            'PART_C_AREA8_8_2_1_SELECTION' => Yii::t('app', 'Part  C  Area8 8 2 1  Selection'),
            'PART_C_AREA8_8_2_2_MGT_STRUCT' => Yii::t('app', 'Part  C  Area8 8 2 2  Mgt  Struct'),
            'PART_C_AREA8_8_2_3_CRITERIA' => Yii::t('app', 'Part  C  Area8 8 2 3  Criteria'),
            'PART_C_AREA8_8_2_4_RLATIONSHIP' => Yii::t('app', 'Part  C  Area8 8 2 4  Rlationship'),
            'PART_C_AREA8_8_2_5_PERFORMANCE' => Yii::t('app', 'Part  C  Area8 8 2 5  Performance'),
            'PART_C_AREA8_8_2_6_CONDUCTIVE' => Yii::t('app', 'Part  C  Area8 8 2 6  Conductive'),
            'PART_C_AREA8_8_3_1_ADMIN_STAFF' => Yii::t('app', 'Part  C  Area8 8 3 1  Admin  Staff'),
            'PART_C_AREA8_8_3_2_NO_ADMIN' => Yii::t('app', 'Part  C  Area8 8 3 2  No  Admin'),
            'PART_C_AREA8_8_3_3_MIN_QUALIFC' => Yii::t('app', 'Part  C  Area8 8 3 3  Min  Qualifc'),
            'PART_C_AREA8_8_3_4_DETAIL_STAF' => Yii::t('app', 'Part  C  Area8 8 3 4  Detail  Staf'),
            'PART_C_AREA8_8_3_5_MECHANISM' => Yii::t('app', 'Part  C  Area8 8 3 5  Mechanism'),
            'PART_C_AREA8_8_3_6_MANAGING' => Yii::t('app', 'Part  C  Area8 8 3 6  Managing'),
            'PART_C_AREA8_8_3_7_TRAINING' => Yii::t('app', 'Part  C  Area8 8 3 7  Training'),
            'PART_C_AREA8_8_3_8_CONDUCT_REG' => Yii::t('app', 'Part  C  Area8 8 3 8  Conduct  Reg'),
            'PART_C_AREA8_8_3_9_SCHEME' => Yii::t('app', 'Part  C  Area8 8 3 9  Scheme'),
            'PART_C_AREA8_8_4_1_SECURE' => Yii::t('app', 'Part  C  Area8 8 4 1  Secure'),
            'PART_C_AREA8_8_4_2_PRIVACY' => Yii::t('app', 'Part  C  Area8 8 4 2  Privacy'),
            'PART_C_AREA8_8_4_3_DEPT_REVIEW' => Yii::t('app', 'Part  C  Area8 8 4 3  Dept  Review'),
            'PART_C_AREA9_INDICATOR' => Yii::t('app', 'Part  C  Area9  Indicator'),
            'PART_C_AREA9_9_1_1_COMPLEMENTS' => Yii::t('app', 'Part  C  Area9 9 1 1  Complements'),
            'PART_C_AREA9_9_1_2_CONTRBUTION' => Yii::t('app', 'Part  C  Area9 9 1 2  Contrbution'),
            'PART_C_AREA9_9_1_3_POLICIES' => Yii::t('app', 'Part  C  Area9 9 1 3  Policies'),
            'PART_C_AREA9_9_1_4_FREQUENCY' => Yii::t('app', 'Part  C  Area9 9 1 4  Frequency'),
            'PART_C_AREA9_9_1_5_RECENT_ACT' => Yii::t('app', 'Part  C  Area9 9 1 5  Recent  Act'),
            'PART_C_AREA9_9_1_6_ROLE' => Yii::t('app', 'Part  C  Area9 9 1 6  Role'),
            'PART_C_AREA9_9_1_7_STEPS' => Yii::t('app', 'Part  C  Area9 9 1 7  Steps'),
            'PART_C_AREA3_INDICATOR' => Yii::t('app', 'Part  C  Area3  Indicator'),
            'PART_C_AREA3_3_1_1_ASSEMNT' => Yii::t('app', 'Part  C  Area3 3 1 1  Assemnt'),
            'PART_C_AREA3_3_1_2_CONSTENT' => Yii::t('app', 'Part  C  Area3 3 1 2  Constent'),
            'PART_C_AREA3_3_1_3_MONITORS' => Yii::t('app', 'Part  C  Area3 3 1 3  Monitors'),
            'PART_C_AREA3_3_1_4_DEPRTMENT' => Yii::t('app', 'Part  C  Area3 3 1 4  Deprtment'),
            'PART_C_AREA3_3_1_5_LEARNING' => Yii::t('app', 'Part  C  Area3 3 1 5  Learning'),
            'PART_C_AREA3_3_2_1_TERMS' => Yii::t('app', 'Part  C  Area3 3 2 1  Terms'),
            'PART_C_AREA3_3_2_2_PRCTICAL' => Yii::t('app', 'Part  C  Area3 3 2 2  Prctical'),
            'PART_C_AREA3_3_2_3_METHDLOGIES' => Yii::t('app', 'Part  C  Area3 3 2 3  Methdlogies'),
            'PART_C_AREA3_3_2_4_MONITORS' => Yii::t('app', 'Part  C  Area3 3 2 4  Monitors'),
            'PART_C_AREA3_3_2_5_REVIEW' => Yii::t('app', 'Part  C  Area3 3 2 5  Review'),
            'PART_C_AREA3_3_2_6_EXT_ASSMNT' => Yii::t('app', 'Part  C  Area3 3 2 6  Ext  Assmnt'),
            'PART_C_AREA3_3_2_7_EXT_EXPRTIS' => Yii::t('app', 'Part  C  Area3 3 2 7  Ext  Exprtis'),
            'PART_C_AREA3_3_3_1_AUTHORITY' => Yii::t('app', 'Part  C  Area3 3 3 1  Authority'),
            'PART_C_AREA3_3_3_2_CREDIBILTY' => Yii::t('app', 'Part  C  Area3 3 3 2  Credibilty'),
            'PART_C_AREA3_3_3_4_CONFDENTIAL' => Yii::t('app', 'Part  C  Area3 3 3 4  Confdential'),
            'PART_C_AREA3_3_3_5_PRFORMANCE' => Yii::t('app', 'Part  C  Area3 3 3 5  Prformance'),
            'PART_C_AREA3_3_3_6_FEEDBACK' => Yii::t('app', 'Part  C  Area3 3 3 6  Feedback'),
            'PART_C_AREA3_3_3_7_RECORDS' => Yii::t('app', 'Part  C  Area3 3 3 7  Records'),
            'PART_C_AREA3_3_3_8_APPEAL' => Yii::t('app', 'Part  C  Area3 3 3 8  Appeal'),
            'PART_C_AREA3_3_3_9_MCHANISM' => Yii::t('app', 'Part  C  Area3 3 3 9  Mchanism'),
            'PART_C_AREA3_3_3_3_COMMITTEES' => Yii::t('app', 'Part  C  Area3 3 3 3  Committees'),
            'PART_C_AREA1_1_2_5_COMPTENCIES' => Yii::t('app', 'Part  C  Area1 1 2 5  Comptencies'),
            'PART_C_AREA2_2_1_2_RELTIONSHP' => Yii::t('app', 'Part  C  Area2 2 1 2  Reltionshp'),
            'PART_C_AREA2_2_2_3_EVIDENCE' => Yii::t('app', 'Part  C  Area2 2 2 3  Evidence'),
            'PART_C_AREA2_2_3_2_OFFER' => Yii::t('app', 'Part  C  Area2 2 3 2  Offer'),
            'PART_C_AREA2_2_5_3_OPPRTNITIES' => Yii::t('app', 'Part  C  Area2 2 5 3  Opprtnities'),
            'PART_C_AREA1_1_1_1_LO' => Yii::t('app', 'Part  C  Area1 1 1 1  Lo'),
            'PART_C_AREA1_1_1_1_OBJ' => Yii::t('app', 'Part  C  Area1 1 1 1  Obj'),
            'PART_C_AREA1_1_2_1_LEARNING' => Yii::t('app', 'Part  C  Area1 1 2 1  Learning'),
            'PART_C_AREA1_1_2_2_MAP' => Yii::t('app', 'Part  C  Area1 1 2 2  Map'),
            'PART_B_6' => Yii::t('app', 'Part  B 6'),
            'PART_C_AREA2_2_3' => Yii::t('app', 'Part  C  Area2 2 3'),
        ];
    }
}
