<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "EC_DOC_ISO".
 *
 * @property string $DOC_ISO_ID
 * @property string $TITLE
 * @property string $FILE_ORI_NAME
 * @property string $FILE_NEW_NAME
 * @property string $FILE_TYPE
 * @property string $FILE_PATH
 * @property string $DOC_ISO_PARENT_ID
 */
class DocIso extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EC_DOC_ISO';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TITLE'], 'required'],
            [['DOC_ISO_PARENT_ID'], 'string'],
            [['TITLE', 'FILE_ORI_NAME', 'FILE_NEW_NAME', 'FILE_TYPE', 'FILE_PATH'], 'string', 'max' => 4000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'DOC_ISO_ID' => Yii::t('app', 'Doc  Iso  ID'),
            'TITLE' => Yii::t('app', 'Title'),
            'FILE_ORI_NAME' => Yii::t('app', 'File  Ori  Name'),
            'FILE_NEW_NAME' => Yii::t('app', 'File  New  Name'),
            'FILE_TYPE' => Yii::t('app', 'File  Type'),
            'FILE_PATH' => Yii::t('app', 'File  Path'),
            'DOC_ISO_PARENT_ID' => Yii::t('app', 'Doc  Iso  Parent  ID'),
        ];
    }
}
