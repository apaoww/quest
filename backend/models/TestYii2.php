<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "TEST_YII2".
 *
 * @property string $TEXT
 * @property string $FIELD
 * @property string $TEST_YII2_ID
 */
class TestYii2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TEST_YII2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TEXT', 'FIELD'], 'string', 'max' => 4000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TEXT' => Yii::t('app', 'Text'),
            'FIELD' => Yii::t('app', 'Field'),
            'TEST_YII2_ID' => Yii::t('app', 'Test  Yii2  ID'),
        ];
    }
}
