<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "EC_FAQ".
 *
 * @property string $FAQ_ID
 * @property string $CONTENT
 * @property string $TITLE
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EC_FAQ';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CONTENT', 'TITLE'], 'string', 'max' => 4000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'FAQ_ID' => Yii::t('app', 'Faq  ID'),
            'CONTENT' => Yii::t('app', 'Content'),
            'TITLE' => Yii::t('app', 'Title'),
        ];
    }
}
