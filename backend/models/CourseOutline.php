<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "EC_COURSE_OUTLINE".
 *
 * @property string $COURSE_OUTLINE_ID
 * @property string $COURSE_ID
 * @property string $DEPARTMENT
 * @property string $SYNOPSIS
 * @property string $PREREQUISITES
 * @property string $COREQUISITES
 * @property string $COURSE_OBJECTIVE
 * @property string $MODE_OF_DELIVERY
 * @property string $TEACH_LEARN_STRATEGIES
 * @property string $REQUIRED_REFERENCE
 * @property string $RECOMMENDED_REFERENCE
 * @property string $EDITABLE
 * @property string $INDICATOR_1
 * @property string $INDICATOR_2
 * @property string $INDICATOR_3
 * @property string $INDICATOR_4
 * @property string $INDICATOR_5
 * @property string $INDICATOR_6
 * @property string $INDICATOR_7
 * @property string $INDICATOR_8
 * @property string $INDICATOR_9
 * @property string $INDICATOR_10
 * @property string $INDICATOR_11
 * @property string $INDICATOR_12
 * @property string $INDICATOR_13
 * @property string $INDICATOR_14
 * @property string $WHO_CAN_EDIT
 * @property string $PARENT_ID
 * @property string $VERSION_ID
 * @property string $CHECKED_BY
 * @property string $APPROVED_BY
 */
class CourseOutline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EC_COURSE_OUTLINE';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['COURSE_ID', 'EDITABLE', 'INDICATOR_1', 'INDICATOR_2', 'INDICATOR_3', 'INDICATOR_4', 'INDICATOR_5', 'INDICATOR_6', 'INDICATOR_7', 'INDICATOR_8', 'INDICATOR_9', 'INDICATOR_10', 'INDICATOR_11', 'INDICATOR_12', 'INDICATOR_13', 'INDICATOR_14', 'PARENT_ID', 'VERSION_ID'], 'string'],
            [['DEPARTMENT', 'SYNOPSIS', 'PREREQUISITES', 'COREQUISITES', 'COURSE_OBJECTIVE', 'MODE_OF_DELIVERY', 'TEACH_LEARN_STRATEGIES', 'REQUIRED_REFERENCE', 'RECOMMENDED_REFERENCE', 'WHO_CAN_EDIT', 'CHECKED_BY', 'APPROVED_BY'], 'string', 'max' => 4000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'COURSE_OUTLINE_ID' => Yii::t('app', 'Course  Outline  ID'),
            'COURSE_ID' => Yii::t('app', 'Course  ID'),
            'DEPARTMENT' => Yii::t('app', 'Department'),
            'SYNOPSIS' => Yii::t('app', 'Synopsis'),
            'PREREQUISITES' => Yii::t('app', 'Prerequisites'),
            'COREQUISITES' => Yii::t('app', 'Corequisites'),
            'COURSE_OBJECTIVE' => Yii::t('app', 'Course  Objective'),
            'MODE_OF_DELIVERY' => Yii::t('app', 'Mode  Of  Delivery'),
            'TEACH_LEARN_STRATEGIES' => Yii::t('app', 'Teach  Learn  Strategies'),
            'REQUIRED_REFERENCE' => Yii::t('app', 'Required  Reference'),
            'RECOMMENDED_REFERENCE' => Yii::t('app', 'Recommended  Reference'),
            'EDITABLE' => Yii::t('app', 'Editable'),
            'INDICATOR_1' => Yii::t('app', 'Indicator 1'),
            'INDICATOR_2' => Yii::t('app', 'Indicator 2'),
            'INDICATOR_3' => Yii::t('app', 'Indicator 3'),
            'INDICATOR_4' => Yii::t('app', 'Indicator 4'),
            'INDICATOR_5' => Yii::t('app', 'Indicator 5'),
            'INDICATOR_6' => Yii::t('app', 'Indicator 6'),
            'INDICATOR_7' => Yii::t('app', 'Indicator 7'),
            'INDICATOR_8' => Yii::t('app', 'Indicator 8'),
            'INDICATOR_9' => Yii::t('app', 'Indicator 9'),
            'INDICATOR_10' => Yii::t('app', 'Indicator 10'),
            'INDICATOR_11' => Yii::t('app', 'Indicator 11'),
            'INDICATOR_12' => Yii::t('app', 'Indicator 12'),
            'INDICATOR_13' => Yii::t('app', 'Indicator 13'),
            'INDICATOR_14' => Yii::t('app', 'Indicator 14'),
            'WHO_CAN_EDIT' => Yii::t('app', 'Who  Can  Edit'),
            'PARENT_ID' => Yii::t('app', 'Parent  ID'),
            'VERSION_ID' => Yii::t('app', 'Version  ID'),
            'CHECKED_BY' => Yii::t('app', 'Checked  By'),
            'APPROVED_BY' => Yii::t('app', 'Approved  By'),
        ];
    }
}
