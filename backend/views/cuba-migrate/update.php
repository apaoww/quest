<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CubaMigrate */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Cuba Migrate',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuba Migrates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cuba-migrate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
