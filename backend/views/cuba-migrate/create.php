<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CubaMigrate */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Cuba Migrate',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cuba Migrates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuba-migrate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
