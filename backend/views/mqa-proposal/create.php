<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MQAProposal */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Mqaproposal',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mqaproposals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mqaproposal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
