<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MQAProposal */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Mqaproposal',
]) . ' ' . $model->MQA_PROPOSAL_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mqaproposals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->MQA_PROPOSAL_ID, 'url' => ['view', 'id' => $model->MQA_PROPOSAL_ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mqaproposal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
