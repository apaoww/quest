<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MQAProposal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mqaproposal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ACAD_ID')->textInput() ?>

    <?= $form->field($model, 'PART_B_1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_3')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_8')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_10')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_11')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_12')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_13')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_14')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_15')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_16')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_17')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_18')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_19')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_1_AIM')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_1_OTHERS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_2_MISSION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_2_SHOWHOW')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_3_JUSTFICTION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_3_MARKETNEED')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_3_RELATE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_4_INCORPORATE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_2')->textInput() ?>

    <?= $form->field($model, 'PART_B_4')->textInput() ?>

    <?= $form->field($model, 'PART_B_5')->textInput() ?>

    <?= $form->field($model, 'PART_B_7')->textInput() ?>

    <?= $form->field($model, 'PART_B_9')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA1_INDICATOR')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_5_CONSULTED')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_2_3_ACHIEVEMENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_2_4_LEARNING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_INDICATOR')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA2_2_1_1_PROVISIONS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_1_3_AUTONOMY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_1_4_POLICIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_1_5_HEP_PLANS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_1_5_DEPT_ROLE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_2_1_PROCESS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_2_2_TEACHGMTHOD')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_2_4_INQUIRY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_2_5_LEARNGMTHOD')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_2_6_MULTI')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_2_7_EXTERNAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_2_8_ACTIVITIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_3_1_SUBJECTS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_3_3_COURSE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_3_4_PLAN')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_3_5_EVIDENCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_4_1_SAMPLE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_4_2_MANNER')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_4_3_DESGNATION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_4_4_REVIEW')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_4_5_LEARNING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_4_6_INITIATIVE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_4_7_ENGAGES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_5_1_LINKS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_5_2_MECHNISM')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_10_COPY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_11_IMPROVING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_12_AUTONMIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_13_NATURE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_INDICATOR')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_1_RESPONSBLE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_2_EVIDENCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_3_ADMISSION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_4_CRITERIA')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_5_APPEAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_6_CHRCTERSTCS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_7_FORECAST')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_8_SELECTION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_9_INTERVIEW')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_10_SPECIAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_11_MONITOR')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_12_ENGAGE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_1_13_RELTIONSHP')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_2_1_POLICIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_2_2_DVLOPMENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_3_1_POLICY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_3_2_ACCEPTED')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_3_3_FACILITIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_1_SUPPORT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_2_ACCESSBLITY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_3_COMPLAIN')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_4_ADEQUACY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_5_ROLES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_6_MGT_RECORD')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_7_ORIENTATED')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_8_SUPPORT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_9_COUNSELLING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_4_10_SPIRITUAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_5_1_ORGANIZED')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_5_2_ACTIVELY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_5_3_SKILLS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_5_4_PUBLICATION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_6_1_ALUMNI')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_6_2_FUTURE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA4_4_6_3_ROLE_ALMNI')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_INDICATOR')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_1_RECRUITMENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_2_TERMS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_3_MIN_QUALFCT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_4_BASIS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_5_PROFILE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_6_STAFFING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_7_CURR_VITAE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_8_PERFORMANCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_9_PROCEDURES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_10_POLICIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_11_STAFF_INFO')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_12_RECRUITMNT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_1_13_RECRUITMNT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_1_DEPARTMNTAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_2_TRAINING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_3_MECHANISM')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_4_EXPERTISE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_5_RESEARCH')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_6_PROFESSIONL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_7_POLICY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_8_MENTORING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_9_COMMUNITY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_10_EVIDENCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_11_OPPORTNITY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_12_RSERCH_ACT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA5_5_2_13_PROVISIONS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_INDICATOR')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_1_PHYSCAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_2_ADEQUACY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_3_UNMEET')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_4_CLINICAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_5_DEMOSTRATE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_6_DATABASE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_7_NO_STAFF')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_8_RESOURCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_9_REFERENCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_10_FEEDBACK')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_11_USE_ICT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_12_ICT_STAFF')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_13_ICT_RQUIRE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_14_PLANS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_15_REVIEWS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_16_OPPRTNTIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_1_17_SPECIAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_2_1_BUDGET')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_2_2_MAJOR')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_2_3_INTERACTION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_2_4_INITIATIVES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_2_5_LINK')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_2_6_REVIEWS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_3_1_POLICY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_3_2_ASSESS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_4_1_PRACTICE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_4_2_DISSEMINATE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_4_3_FUTURE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_4_4_FINANCIAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_5_1_RSPONSBLITS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_5_2_ALLOCTION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA6_6_5_3_RESPONSIBLE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_INDICATOR')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA7_7_1_1_EVALUATES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_1_2_PROGRESSION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_1_3_MONITORING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_1_4_ACHIEVEMENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_1_5_UTILISES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_1_6_STRUCTURE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_1_7_RSPNSBILITS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_1_8_SELF_REVIEW')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_1_9_MECHANISM')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_2_1_STAKEHOLDER')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_2_2_CONSDERTION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_2_3_INFORMS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_2_4_FEEDBACKS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA7_7_2_5_PROFSSIONAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_INDICATOR')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA8_8_1_1_POLICIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_1_2_GOVERNANCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_1_3_FREQUENCY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_1_4_EVIDENCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_1_5_AUTONOMY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_1_6_COMMITTEE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_1_7_RPRSNTATION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_2_1_SELECTION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_2_2_MGT_STRUCT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_2_3_CRITERIA')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_2_4_RLATIONSHIP')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_2_5_PERFORMANCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_2_6_CONDUCTIVE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_3_1_ADMIN_STAFF')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_3_2_NO_ADMIN')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_3_3_MIN_QUALIFC')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_3_4_DETAIL_STAF')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_3_5_MECHANISM')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_3_6_MANAGING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_3_7_TRAINING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_3_8_CONDUCT_REG')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_3_9_SCHEME')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_4_1_SECURE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_4_2_PRIVACY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA8_8_4_3_DEPT_REVIEW')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA9_INDICATOR')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA9_9_1_1_COMPLEMENTS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA9_9_1_2_CONTRBUTION')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA9_9_1_3_POLICIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA9_9_1_4_FREQUENCY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA9_9_1_5_RECENT_ACT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA9_9_1_6_ROLE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA9_9_1_7_STEPS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_INDICATOR')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA3_3_1_1_ASSEMNT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_1_2_CONSTENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_1_3_MONITORS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_1_4_DEPRTMENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_1_5_LEARNING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_2_1_TERMS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_2_2_PRCTICAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_2_3_METHDLOGIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_2_4_MONITORS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_2_5_REVIEW')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_2_6_EXT_ASSMNT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_2_7_EXT_EXPRTIS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_1_AUTHORITY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_2_CREDIBILTY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_4_CONFDENTIAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_5_PRFORMANCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_6_FEEDBACK')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_7_RECORDS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_8_APPEAL')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_9_MCHANISM')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA3_3_3_3_COMMITTEES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_2_5_COMPTENCIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_1_2_RELTIONSHP')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_2_3_EVIDENCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_3_2_OFFER')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA2_2_5_3_OPPRTNITIES')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_1_LO')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_1_1_OBJ')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_2_1_LEARNING')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_C_AREA1_1_2_2_MAP')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PART_B_6')->textInput() ?>

    <?= $form->field($model, 'PART_C_AREA2_2_3')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
