<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TestYii2 */

$this->title = $model->TEST_YII2_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Yii2s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-yii2-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->TEST_YII2_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->TEST_YII2_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'TEXT:ntext',
            'FIELD',
            'TEST_YII2_ID',
        ],
    ]) ?>

</div>
