<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TestYii2 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Test Yii2',
]) . ' ' . $model->TEST_YII2_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Yii2s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TEST_YII2_ID, 'url' => ['view', 'id' => $model->TEST_YII2_ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="test-yii2-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
