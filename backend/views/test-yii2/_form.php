<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TestYii2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="test-yii2-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'TEXT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'FIELD')->textInput(['maxlength' => 4000]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
