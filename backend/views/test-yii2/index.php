<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Test Yii2s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-yii2-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Test Yii2',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'TEXT:ntext',
            'FIELD',
            'TEST_YII2_ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
