<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'New Acads');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="new-acad-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create New Acad'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ACAD_ID',
            'LEVEL_ID',
            'AREA_SPECIALIZATION_ID',
            'KULY',
            'NAME_PROG_ENG',
            // 'NAME_PROG_MAL',
            // 'EXPECTED_YEAR_OFFERED',
            // 'JUSTIFICATION',
            // 'IPTA_ID',
            // 'HR_IMPLICATION_ID',
            // 'PR_IMPLICATION_ID',
            // 'FIN_IMPLICATION_ID',
            // 'ACAD_STATUS',
            // 'SUB_AREA_SPECIALIZATION_ID',
            // 'STATUS_ID',
            // 'INSERT_BY',
            // 'DATE_INSERT',
            // 'INDICATOR_LEVELID',
            // 'INDICATOR_AREA_SPECIALIZATION',
            // 'INDICATOR_KULY',
            // 'INDICATOR_NAME_PROG_ENG',
            // 'INDICATOR_NAME_PROG_MAL',
            // 'INDICATOR_EXPECTED_YEAR',
            // 'INDICATOR_JUSTIFICATION',
            // 'INDICATOR_IPTA',
            // 'INDICATOR_HR_IMPLICATION',
            // 'INDICATOR_PR_IMPLICATION',
            // 'INDICATOR_FIN_IMPLICATION',
            // 'INDICATOR_OVERLAPPING',
            // 'INDICATOR_OBJECTIVE',
            // 'EC_OVERLAPPING_DESC:ntext',
            // 'PARENT_ID',
            // 'WHO_CAN_EDIT',
            // 'GROUP_ID',
            // 'DEPT_CODE',
            // 'MQA_APPROVE_DATE',
            // 'SENATE_APPROVE_DATE',
            // 'JPT_APPROVE_DATE',
            // 'MAJLIS_APPROVE_DATE',
            // 'PROFESSIONAL_BODY_APPROVE_DATE',
            // 'MQR_NUMBER',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
