<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\NewAcad */

$this->title = $model->ACAD_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'New Acads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="new-acad-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ACAD_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ACAD_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ACAD_ID',
            'LEVEL_ID',
            'AREA_SPECIALIZATION_ID',
            'KULY',
            'NAME_PROG_ENG',
            'NAME_PROG_MAL',
            'EXPECTED_YEAR_OFFERED',
            'JUSTIFICATION',
            'IPTA_ID',
            'HR_IMPLICATION_ID',
            'PR_IMPLICATION_ID',
            'FIN_IMPLICATION_ID',
            'ACAD_STATUS',
            'SUB_AREA_SPECIALIZATION_ID',
            'STATUS_ID',
            'INSERT_BY',
            'DATE_INSERT',
            'INDICATOR_LEVELID',
            'INDICATOR_AREA_SPECIALIZATION',
            'INDICATOR_KULY',
            'INDICATOR_NAME_PROG_ENG',
            'INDICATOR_NAME_PROG_MAL',
            'INDICATOR_EXPECTED_YEAR',
            'INDICATOR_JUSTIFICATION',
            'INDICATOR_IPTA',
            'INDICATOR_HR_IMPLICATION',
            'INDICATOR_PR_IMPLICATION',
            'INDICATOR_FIN_IMPLICATION',
            'INDICATOR_OVERLAPPING',
            'INDICATOR_OBJECTIVE',
            'EC_OVERLAPPING_DESC:ntext',
            'PARENT_ID',
            'WHO_CAN_EDIT',
            'GROUP_ID',
            'DEPT_CODE',
            'MQA_APPROVE_DATE',
            'SENATE_APPROVE_DATE',
            'JPT_APPROVE_DATE',
            'MAJLIS_APPROVE_DATE',
            'PROFESSIONAL_BODY_APPROVE_DATE',
            'MQR_NUMBER',
        ],
    ]) ?>

</div>
