<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NewAcad */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'New Acad',
]) . ' ' . $model->ACAD_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'New Acads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ACAD_ID, 'url' => ['view', 'id' => $model->ACAD_ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="new-acad-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
