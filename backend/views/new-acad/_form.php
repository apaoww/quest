<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NewAcad */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="new-acad-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'LEVEL_ID')->textInput() ?>

    <?= $form->field($model, 'AREA_SPECIALIZATION_ID')->textInput() ?>

    <?= $form->field($model, 'KULY')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'NAME_PROG_ENG')->textInput(['maxlength' => 2000]) ?>

    <?= $form->field($model, 'NAME_PROG_MAL')->textInput(['maxlength' => 2000]) ?>

    <?= $form->field($model, 'EXPECTED_YEAR_OFFERED')->textInput() ?>

    <?= $form->field($model, 'JUSTIFICATION')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'IPTA_ID')->textInput() ?>

    <?= $form->field($model, 'HR_IMPLICATION_ID')->textInput() ?>

    <?= $form->field($model, 'PR_IMPLICATION_ID')->textInput() ?>

    <?= $form->field($model, 'FIN_IMPLICATION_ID')->textInput() ?>

    <?= $form->field($model, 'ACAD_STATUS')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'SUB_AREA_SPECIALIZATION_ID')->textInput() ?>

    <?= $form->field($model, 'STATUS_ID')->textInput() ?>

    <?= $form->field($model, 'INSERT_BY')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'DATE_INSERT')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_LEVELID')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_AREA_SPECIALIZATION')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_KULY')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_NAME_PROG_ENG')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_NAME_PROG_MAL')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_EXPECTED_YEAR')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_JUSTIFICATION')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_IPTA')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_HR_IMPLICATION')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_PR_IMPLICATION')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_FIN_IMPLICATION')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_OVERLAPPING')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_OBJECTIVE')->textInput() ?>

    <?= $form->field($model, 'EC_OVERLAPPING_DESC')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PARENT_ID')->textInput() ?>

    <?= $form->field($model, 'WHO_CAN_EDIT')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'GROUP_ID')->textInput() ?>

    <?= $form->field($model, 'DEPT_CODE')->textInput() ?>

    <?= $form->field($model, 'MQA_APPROVE_DATE')->textInput() ?>

    <?= $form->field($model, 'SENATE_APPROVE_DATE')->textInput() ?>

    <?= $form->field($model, 'JPT_APPROVE_DATE')->textInput() ?>

    <?= $form->field($model, 'MAJLIS_APPROVE_DATE')->textInput() ?>

    <?= $form->field($model, 'PROFESSIONAL_BODY_APPROVE_DATE')->textInput() ?>

    <?= $form->field($model, 'MQR_NUMBER')->textInput(['maxlength' => 200]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
