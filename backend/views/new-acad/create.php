<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\NewAcad */

$this->title = Yii::t('app', 'Create New Acad');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'New Acads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="new-acad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
