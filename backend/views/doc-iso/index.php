<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Doc Isos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doc-iso-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Doc Iso',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'DOC_ISO_ID',
            'TITLE',
            'FILE_ORI_NAME',
            'FILE_NEW_NAME',
            'FILE_TYPE',
            // 'FILE_PATH',
            // 'DOC_ISO_PARENT_ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
