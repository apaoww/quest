<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DocIso */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Doc Iso',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Doc Isos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doc-iso-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
