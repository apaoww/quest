<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DocIso */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Doc Iso',
]) . ' ' . $model->TITLE;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Doc Isos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TITLE, 'url' => ['view', 'id' => $model->DOC_ISO_ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="doc-iso-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
