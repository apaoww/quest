<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DocIso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="doc-iso-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'TITLE')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'FILE_ORI_NAME')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'FILE_NEW_NAME')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'FILE_TYPE')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'FILE_PATH')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'DOC_ISO_PARENT_ID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
