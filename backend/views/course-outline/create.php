<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CourseOutline */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Course Outline',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Course Outlines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-outline-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
