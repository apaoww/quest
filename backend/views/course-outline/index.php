<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Course Outlines');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-outline-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Course Outline',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'COURSE_OUTLINE_ID',
            'COURSE_ID',
            'DEPARTMENT',
            'SYNOPSIS',
            'PREREQUISITES',
            // 'COREQUISITES',
             'COURSE_OBJECTIVE:ntext',
            // 'MODE_OF_DELIVERY',
            // 'TEACH_LEARN_STRATEGIES',
            // 'REQUIRED_REFERENCE:ntext',
            // 'RECOMMENDED_REFERENCE:ntext',
            // 'EDITABLE',
            // 'INDICATOR_1',
            // 'INDICATOR_2',
            // 'INDICATOR_3',
            // 'INDICATOR_4',
            // 'INDICATOR_5',
            // 'INDICATOR_6',
            // 'INDICATOR_7',
            // 'INDICATOR_8',
            // 'INDICATOR_9',
            // 'INDICATOR_10',
            // 'INDICATOR_11',
            // 'INDICATOR_12',
            // 'INDICATOR_13',
            // 'INDICATOR_14',
            // 'WHO_CAN_EDIT',
            // 'PARENT_ID',
            // 'VERSION_ID',
            // 'CHECKED_BY:ntext',
            // 'APPROVED_BY:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
