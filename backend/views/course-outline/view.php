<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CourseOutline */

$this->title = $model->COURSE_OUTLINE_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Course Outlines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-outline-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->COURSE_OUTLINE_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->COURSE_OUTLINE_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'COURSE_OUTLINE_ID',
            'COURSE_ID',
            'DEPARTMENT',
            'SYNOPSIS:ntext',
            'PREREQUISITES',
            'COREQUISITES',
            'COURSE_OBJECTIVE:ntext',
            'MODE_OF_DELIVERY',
            'TEACH_LEARN_STRATEGIES',
            'REQUIRED_REFERENCE:ntext',
            'RECOMMENDED_REFERENCE:ntext',
            'EDITABLE',
            'INDICATOR_1',
            'INDICATOR_2',
            'INDICATOR_3',
            'INDICATOR_4',
            'INDICATOR_5',
            'INDICATOR_6',
            'INDICATOR_7',
            'INDICATOR_8',
            'INDICATOR_9',
            'INDICATOR_10',
            'INDICATOR_11',
            'INDICATOR_12',
            'INDICATOR_13',
            'INDICATOR_14',
            'WHO_CAN_EDIT',
            'PARENT_ID',
            'VERSION_ID',
            'CHECKED_BY:ntext',
            'APPROVED_BY:ntext',
        ],
    ]) ?>

</div>
