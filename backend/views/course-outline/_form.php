<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CourseOutline */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-outline-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'COURSE_ID')->textInput() ?>

    <?= $form->field($model, 'DEPARTMENT')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'SYNOPSIS')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'PREREQUISITES')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'COREQUISITES')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'COURSE_OBJECTIVE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'MODE_OF_DELIVERY')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'TEACH_LEARN_STRATEGIES')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'REQUIRED_REFERENCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'RECOMMENDED_REFERENCE')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'EDITABLE')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_1')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_2')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_3')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_4')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_5')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_6')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_7')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_8')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_9')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_10')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_11')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_12')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_13')->textInput() ?>

    <?= $form->field($model, 'INDICATOR_14')->textInput() ?>

    <?= $form->field($model, 'WHO_CAN_EDIT')->textInput(['maxlength' => 4000]) ?>

    <?= $form->field($model, 'PARENT_ID')->textInput() ?>

    <?= $form->field($model, 'VERSION_ID')->textInput() ?>

    <?= $form->field($model, 'CHECKED_BY')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'APPROVED_BY')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
