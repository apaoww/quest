<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CourseOutline */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Course Outline',
]) . ' ' . $model->COURSE_OUTLINE_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Course Outlines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->COURSE_OUTLINE_ID, 'url' => ['view', 'id' => $model->COURSE_OUTLINE_ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="course-outline-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
